<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable=[
        'title',
        'content',
        'publishedAt',
        'image',
    ];
    // materialisation des relations
    // fonction qui lie un post a une et une seule ue

//     public function concerner(){
//         $this->belongsTo('app\Ue');
//     }
 }
