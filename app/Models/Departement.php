<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    use HasFactory;
    // methodes qui retourne tous les membres d'un departement
    public function users() {
        $this->hasMany('app\user');
    }
    
}
