<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ue extends Model
{
    use HasFactory;
    // fonction qui retourne la liste des messages contenus dans une ue
    public function posts(){
        $this->hasMany('app\Post');
    }
}
