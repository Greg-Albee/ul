<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->string('image');
            $table->bigInteger('idUe')->unsigned()->nullable(); 
            $table->bigInteger('idDepartement')->unsigned()->nullable(); 
            $table->bigInteger('idUser')->unsigned()->nullable(); 
            $table->foreign('idUe')->references('id')->on('ues');
            $table->foreign('idDepartement')->references('id')->on('departements');
	        $table->foreign('idUser')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
