<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtudiantUesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etudiant_ues', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('idUes')->unsigned();
            $table->bigInteger('idUsers')->unsigned();
            $table->foreign('idUes')->references('id')->on('ues');
            $table->foreign('idUsers')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etudiant_ues');
    }
}
