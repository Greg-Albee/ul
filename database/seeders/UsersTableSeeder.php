<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User();
        $user->identifiant = 'ilarie1';
        $user->nom = 'Tossou';
        $user->prenom = 'Ilarie';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'ilarietossou@gmail.com';
        $user->role = 'Etudiant';
        $user->save();
        

        $user = new User();
        $user->identifiant = 'robert1';
        $user->nom = "d'almeida";
        $user->prenom = 'Kokou Robert';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'robertdalmeida@gmail.com';
        $user->role = 'Etudiant';
        $user->save();

        $user = new User();
        $user->identifiant = 'christian1';
        $user->nom = 'Ametoh';
        $user->prenom = 'Fred Christian';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'christametoh@hotmail.com';
        $user->role = 'Etudiant';
        $user->save();

        $user = new User();
        $user->identifiant = 'schalom1';
        $user->nom = 'Alihonou';
        $user->prenom = 'Sibi Schalom';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'schalomalihonou@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'joel1';
        $user->nom = 'Richardson';
        $user->prenom = 'Serge Joel';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'joelRichardson@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'clementine1';
        $user->nom = 'Fynn';
        $user->prenom = 'Lionella Clementine';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'clementinefynn@gmail.com';
        $user->role = 'Secrétaire';
        $user->save();

        $user = new User();
        $user->identifiant = 'victor1';
        $user->nom = 'La chèvre';
        $user->prenom = 'Victor';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'victorLaChevre@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'joseph1';
        $user->nom = 'KPOGNON';
        $user->prenom = 'Joseph';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'josephkpognon@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'marie1';
        $user->nom = 'DEGBEVI';
        $user->prenom = 'Marie';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'mariedegbevi@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'pacôme1';
        $user->nom = 'KPODAR';
        $user->prenom = 'Pacôme';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'pacomekpodar@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'david1';
        $user->nom = 'AFANDOLO';
        $user->prenom = 'David';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'davidafandolo@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'fortuné1';
        $user->nom = 'AKPEMAGNON';
        $user->prenom = 'Fortuné';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'fortuneakpemagnon@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'paul1';
        $user->nom = 'NOUMONVI';
        $user->prenom = 'Paul';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'paulnoumonvi@gmail.com';
        $user->role = 'Secrétaire';
        $user->save();

        $user = new User();
        $user->identifiant = 'rosalie1';
        $user->nom = 'FOLLY';
        $user->prenom = 'Rosalie';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'rosaliefolly@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'albert1';
        $user->nom = 'AGBO';
        $user->prenom = 'Albert';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'albertagbo@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'linda1';
        $user->nom = 'TOGBUI-GNAMAVON';
        $user->prenom = 'Linda';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'lindatogbui@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'jean1';
        $user->nom = 'GAGNANTO';
        $user->prenom = 'jeangagnanto';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'jeangagnanto@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'frédéric1';
        $user->nom = 'ALEVI';
        $user->prenom = 'Fréderic';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'fredericalevi@gmail.com';
        $user->role = 'Etudiant';
        $user->save();

        $user = new User();
        $user->identifiant = 'harmonie1';
        $user->nom = 'TCHOEKPO';
        $user->prenom = 'Harmonie';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'harmonietchoekpo@gmail.com';
        $user->role = 'Enseignant';
        $user->save();

        $user = new User();
        $user->identifiant = 'monique1';
        $user->nom = 'JUMA';
        $user->prenom = 'Ilarie';
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->idDepartement = 1;
        $user->email = 'moniquejuma@gmail.com';
        $user->role = 'Enseignant';
        $user->save();
    }
}
