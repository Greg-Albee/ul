<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Etudiant_ue;

class Etudiant_ueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 14;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 2;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 3;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 4;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 5;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1; 
        $etudiant_ue->idUes = 6;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 7;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 8;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 9;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 10;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 11;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 12;
        $etudiant_ue->save();

        $etudiant_ue = new Etudiant_ue();
        $etudiant_ue->idUsers = 1;
        $etudiant_ue->idUes = 13;
        $etudiant_ue->save();
    }
}
