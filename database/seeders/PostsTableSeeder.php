<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $post = new Post();
        $post->title = 'title';
        $post->content = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earum culpa molestias, laboriosam incidunt modi similique expedita 
        hic nihil dolore aperiam natus odit cumque fuga magnam id facilis ad eum vero!';
        $post->image = 'ul.png';
        $post->save();
    }
}
