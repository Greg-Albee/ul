<?php

namespace Database\Seeders;

use App\Models\Ue;
use Illuminate\Database\Seeder;

class UesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $ue = new ue();
        $ue->code_ue = 'BIO100';
        $ue->libelle = 'Biologie cellulaire'; 
        $ue->semestre = 1;
        $ue->id = 6;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'BPH106';
        $ue->libelle = 'Biophysique générale'; 
        $ue->semestre = 1;
        $ue->id = 2;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'CHM108';
        $ue->libelle = 'Chimie générale'; 
        $ue->semestre = 1;
        $ue->id = 3;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'CHM144';
        $ue->libelle = 'Chimie organique'; 
        $ue->semestre = 1;
        $ue->id = 4;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'INF131';
        $ue->libelle = 'Informatique'; 
        $ue->semestre = 1;
        $ue->id = 5;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'MTH160';
        $ue->libelle = 'Mathématiques-statiques'; 
        $ue->semestre = 1;
        $ue->id = 7;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'PHY100';
        $ue->libelle = 'Physique'; 
        $ue->semestre = 1;
        $ue->id = 8;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'ANA101';
        $ue->libelle = 'Anatomie1'; 
        $ue->semestre = 2;
        $ue->id = 9;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'BCH210';
        $ue->libelle = 'Biologie générale'; 
        $ue->semestre = 2;
        $ue->id = 10;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'BOT109';
        $ue->libelle = 'Génétique'; 
        $ue->semestre = 2;
        $ue->id = 11;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'BIO202';
        $ue->libelle = 'Génétique'; 
        $ue->semestre = 2;
        $ue->id = 12;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'BIO110';
        $ue->libelle = 'Parasitologie-Entomologie'; 
        $ue->semestre = 2;
        $ue->id = 13;
        $ue->save();

        $ue = new ue();
        $ue->code_ue = 'BIO203';
        $ue->libelle = 'Physiologie générale'; 
        $ue->semestre = 2;
        $ue->id = 14;
        $ue->save();
    }
}
