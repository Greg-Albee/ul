<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Departement;

class DepartementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $departement = new Departement();
        $departement->code_depart='FASEG';
        $departement->nameDepartement ='Faculté des Sciences et de Gestion';
        $departement->save();

        $departement = new Departement();
        $departement->code_depart='FSS';
        $departement->nameDepartement ='Faculté des Sciences et de la Santé';
        $departement->save();
    }
}
