@extends('layouts.base')

@section('content')
<div id="pageintro" class="hoc clear"> 
  <!-- ################################################################################################ -->
  <div class="flexslider basicslider">
    <ul class="slides">
      <li>
          {{-- <p>Donec rhoncus ullamcorper</p> --}}
          <p>&laquo; Celui qui détient l'information, détient le pouvoir.
            Celui qui l'entretien, détient le monde. &raquo; Adam Smires</p>
          <footer><a class="btn" href="#">Consulter sa boîte d'informations</a></footer>
        
      </li>
      {{-- <li>
        <article>
          <p>Sapien aenean eu lectus</p>
            <h3 class="heading">Fusce mauris molestie magna</h3>
            <p>Nascetur urna lectus dignissim eget convallis pulvinar commodo nec felis cum sociis natoque penatibus et magnis dis parturient montes.</p>
            <footer><a class="btn" href="#">Ridiculus mus</a></footer>
          </article>
        </li> 
      <li>
        <article>
          <p>Justo lectus faucibus</p>
          <h3 class="heading">Neque a mollis augue arcu at pede</h3>
          <p>Integer molestie sapien ac enim praesent non justo phasellus quam nulla facilisi praesent massa lorem feugiat eget gravida vitae.</p>
          <footer><a class="btn" href="#">Vivamus convallis</a></footer>
        </article>
      </li> --}}
    </ul>
  </div>
  <!-- ################################################################################################ -->
</div>

<!-- ################################################################################################ -->
</div>
<!-- End Top Background Image Wrapper -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
<main class="hoc container clear"> 
  <!-- main body -->
  <!-- ################################################################################################ -->
  <div class="group">
    <div class="one_half first">
      <p class="font-xs">Article</p>
      <h6 class="heading font-x3">Suspension des cours en présentiel</h6>
      <p>Il est porté à l'intention de toute la communauté estudiante que dans le but de dimunier la progression de la pandémie, les cours sont suspendus jusqu'à nouvel ordre.</p>
      <p>Les cours se feront désormais de manière asynchrone sur [&hellip;]</p>
      <footer><a href="#">Voir plus &raquo;</a></footer>
    </div>
    <div class="one_half"><a href="#"><img src="images/1.png" alt=""></a></div>
  </div>
  <!-- ################################################################################################ -->
  <!-- / main body -->
  <div class="clear"></div>
</main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded" style="  background-image:url('images/f.jpg'); background-size:45%; background-position:left;">
<div class="hoc split clear">
  <section> 
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">Restaurant Universitaire</h6>
      <p>Le menu au restaurant de la semaine </p>
    </div>
    <div class="group">
      <article class="tree_quarter first">
        <h4 class="heading font-x1">NOURRITURE</h4>
        <p>“La nourriture la meilleure est celle qui contient le plus de calories.” [&hellip;]</p>
        <footer>
          <a class="btn" href="#">voir &raquo;</a>
          <pre style="display:inline;">              </pre>
          <a class="btn" href="#">Télécharger &raquo;</a>
        </footer>
      {{-- </article>
      <article class="one_half">
        <h4 class="heading font-x1">Placerat diam donec tortor</h4>
        <p>Lectus varius vel egestas a dictum in odio ut nisi aenean ac nisl vitae ipsum lobortis ultricies ut tincidunt magna in eros integer [&hellip;]</p> --}}
        <footer></footer>
      </article>
    </div>
    <!-- ################################################################################################ -->
  </section>
</div>
</div> 
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="group">
      <div class="one_half first">
        <p class="font-xs">Direction des Affaires Académiques et de la Scolarité</p>
        <h6 class="heading font-x3">Lancement des concours </h6>
        <p>Il est organisé à l’Université de Lomé, un concours de recrutement d’étudiants pour la formation en Licence Professionnelles filières Génie Logiciel et Maintenance et Réseau Informatique au Centre Informatique et de Calcul (CIC).

Pourront se présenter à ce concours, les candidats titulaires du baccalauréat scientifiques deuxième partie (BAC II) des séries C, D, E, et F ou de tout autre diplôme équivalent, vieux d’un an au plus (Baccalauréat 2ème partie 2019).

Le dossier de candidature doit comporter :

• une (01) demande manuscrite adressée à Monsieur le Directeur du CIC (préciser la filière choisie) ;
• un (01) curriculum vitae ;
• une (01) copie légalisée du certificat de nationalité (original à présenter sur place);
• un (01) extrait de l’acte de naissance ou une fiche d’état-civil ;
.</p>
        <footer><a href="#">Voir plus &raquo;</a></footer>
      </div>
      <div class="one_half"><a href="#"><img src="images/3.jpg" alt=""></a></div>
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
  </div>
@endsection
