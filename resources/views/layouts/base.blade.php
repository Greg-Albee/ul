<!DOCTYPE html>
<!--
Template Name: Mudcappro
Author: <a href="http://www.os-templates.com/">OS Templates</a>
Author URI: http://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: http://www.os-templates.com/template-terms
-->
<html>
<head>
<title>{{ config('app.name', 'Laravel') }}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row1">
@include('layouts.partials.header') 
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Top Background Image Wrapper -->
@if(!Route::is('boite'))
  <div class="bgded overlay" style="background-image:url('images/demo/backgrounds/2.png'); background-size:100%;"> 
@endif
  <!-- ################################################################################################ -->
  <div class="bgded overlay">
    <div class="wrapper row2">
      @include('layouts.partials.nav')
    </div>
  </div>
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
@yield('content')
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Footer Background Image Wrapper -->
@include('layouts.partials.footer')
<!-- JAVASCRIPTS -->
@include('layouts.partials.js')
</body>
</html>