<div class="bgded overlay">
    <!-- ################################################################################################ -->
    <div class="wrapper row4">
      <footer id="footer" class="hoc clear"> 
        <!-- ################################################################################################ -->
        {{-- <div class="one_quarter first" style="color : white; ">
          <h6 class="heading">Ul-Info</h6>
        </div> --}}
        <div class="two_quarter first" style="color : white; ">
          <h6 class="heading">Présentation</h6>
          <nav>
            <ul class="nospace">
              <li><a href="#"><i class="fa fa-lg fa-home"></i></a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
          </nav>
          <ul class="faico clear">
            <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
            <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a class="faicon-vk" href="#"><i class="fa fa-vk"></i></a></li>
          </ul>
        </div>
        {{-- <div class="one_quarter" style="color : white; ">
          <h6 class="heading">Suspendisse potenti</h6>
          <article>
            <h2 class="nospace font-x1"><a href="#">Erat volutpat nam</a></h2>
            <time class="font-xs" datetime="2045-04-06">Friday, 6<sup>th</sup> April 2045</time>
            <p>Massa ac semper nibh sem eu quam sed id nisl sed pulvinar ligula in turpis proin nisl purus iaculis eget et bibendum quis dictum lorem tellus.</p>
          </article>
        </div> --}}
        <div class="one_quarter" style="color : white; ">
          <h6 class="heading">Liens</h6>
          <ul class="nospace linklist">
            <li><a href="#"><p>http://www.etu.univ-lome.tg</p></a></li>
            <li><a href="#"><p>https://elearn.univ-lome.tg</p></a></li>
          </ul>
        </div>
        <!-- ################################################################################################ -->
      </footer>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="wrapper row5" style="color : white; ">
      <div id="copyright" class="hoc clear"> 
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy; 2021 - All Rights Reserved</p>
        <p class="fl_right"><a target="_blank" href="http://www.etu.univ-lome.tg/" title="Free Website Templates">Université de Lomé</a></p>
        <!-- ################################################################################################ -->
      </div>
    </div>
    <!-- ################################################################################################ -->
  </div>
  <!-- End Footer Background Image Wrapper -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>