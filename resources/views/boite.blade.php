@extends('layouts.base')
@section('content')
<section>
    <div>
        <h3>UNITES D'ENSEIGNEMENT</h3>
            <ul>
                <li>
                    {{-- lecture des u.e depuis la base de données --}}
                </li>
            </ul>
    </div>
 </section>  

</div>
<h3>INFORMATION</h3>
    @foreach ($posts as $post)
    <section>
        <div>
            <div class="card" style="width: 18rem;">
                <img src="{{asset('images/'.$post->image)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{$post->title}}</h5>
                  <p class="card-text">{{$post->content}}</p>
                </div>
              </div>
        </div>
    </section> 
    @endforeach
@endsection